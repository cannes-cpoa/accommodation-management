<?php
/*
 * General Configuration
 */
define('WEBSITE_NAME', 'AccomodationManager');      // Website name
/*
 * Lang
 */
define('LANG_DEFAULT', 'fr');                       // Default lang
define('LANG_PATH', 'src/lang/');
/*
 * Routing Configuration
 */
define('WEBSITE_DEFAULT_PATH', 'index');
define('WEBSITE_PATH', '/AccomodationCPOA/');        // "/" for "https://website/" or "/sourcepath/" for "https://website/sourcepath/"
/*
 * Database Configuration
 */
define('DB_HOST', "localhost");
define('DB_USER', "root");
define('DB_PASSWORD', "");
define('DB_NAME', "cannes");
/*
 * Engine Path
 */
define('CONTROLLER_PATH','controller/');
define('MODELS_PATH','models/');
define('VIEW_PATH','view/');