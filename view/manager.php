<?php
require_once('template/head.php');
?>
    <div class="container-fluid section">
        <div class="row">
            <div class="col-md-10"><h2>Gestions des réservations</h2></div>
            <div class="col-md-2"><a href="<?=genURL('reservation')?>" class="btn btn-success">Ajouter une Reservation</a></div>
        </div>

        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nom de logement</th>
                <th scope="col">Adresse</th>
                <th scope="col">Type</th>
                <th scope="col">Service(s)</th>
                <th scope="col">Nombre de Reservation(s)</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($hotels as $hotel){
                ?>
                <tr>
                    <th scope="row"><?=$hotel->getId()?></th>
                    <td><?=htmlspecialchars($hotel->getName())?></td>
                    <td><?=htmlspecialchars($hotel->getAddress()) . ', ' . $hotel->getPostalCode() ?></td>
                    <td><?=htmlspecialchars($hotel->getType())?></td>
                    <td><?=$hotel->getServices() ? htmlspecialchars(join(',',$hotel->getServices())) : "Aucun" ?></td>
                    <td><?=count(AccomodationReservation::fetchByAccomodationId($hotel->getId()))?></td>
                    <td><a href="<?=genURL('manager/' . $hotel->getId() . '/view')?>" class="btn btn-primary">Editer une reservation</a></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
<?php
require_once('template/footer.php');
?>