<?php
require_once('template/head.php');
?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <div class="container-fluid section">
        <h2>Ajout d'une reservation</h2>
    </div>
    <div class="container-fluid section">
        <?=$alert?>
        <div class="row justify-content-md-center">
            <div class="col-md-6 col-md-offset-3">
                <form id="msform" method="POST" action="">
                    <ul id="progressbar">
                        <li class="active">VIP</li>
                        <li>Reservation</li>
                        <li>Logement</li>
                    </ul>
                    <fieldset>
                        <h2 class="fs-title">VIP</h2>
                        <h3 class="fs-subtitle">Choix du VIP</h3>
                        <input type="search" id="users-filter" placeholder="Rechercher..." name="vip" data-list="users-list" autocomplete="off" required />
                        <label for="users-filter" data-icon="&#128269;"></label>
                        <datalist id="users-list">
                            <select>
                                <?php
                                forEach($vips as $vip){
                                    echo '<option value="' .$vip->getEmail(). '" />';
                                }
                                ?>
                            </select>
                        </datalist>
                        <input type="button" name="next" class="next action-button" value="Suivant"/>
                    </fieldset>
                    <fieldset>
                        <h2 class="fs-title">Reservation</h2>
                        <h3 class="fs-subtitle">Détail de la reservation</h3>
                        Nombres d'occupants<input type="text" id="people" name="people" placeholder="1"/>
                        Nombres de chambres<input type="text" id="rooms" name="rooms" placeholder="1"/>
                        Date d'entrée<input type="text" name="dateStart" id="dateStart"/>
                        Date de sortie<input type="text" name="dateEnd" id="dateEnd"/>
                        <input type="button" name="previous" class="previous action-button-previous" value="Retour"/>
                        <input type="button" name="next" class="next action-button" value="Suivant"/>
                    </fieldset>
                    <fieldset>
                        <h2 class="fs-title">Logement</h2>
                        <h3 class="fs-subtitle">Choix du logement</h3>
                        <input type="search" id="location-filter" placeholder="Rechercher..." name="location" autocomplete="off" required />
                        <label for="location-filter" data-icon="&#128269;"></label>
                        <input type="button" name="previous" class="previous action-button-previous" value="Retour"/>
                        <button type="submit" class="submit action-button">Ajouter</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script>
        var datesReserve = [];
        var locations = [];
        /*
        Event choix VIP
        */
        $(document).ready(function() {
            $("#users-filter").change(function (e){
                $.get( '<?=genURL('reservation/date/')?>' + $("#users-filter").val(), function( data ) {
                    datesReserve = [];
                    datesReserve = datesReserve.concat(JSON.parse(data));
                    reloadDate();
                });
            });
            $("#dateEnd").change(function (e){
                $.get( '<?=genURL('reservation/hotel/')?>' + $("#people").val() + '/'  + $("#rooms").val() + '/' + $("#dateStart").val() + '/' + $("#dateEnd").val() , function( data ) {
                    locations = [];
                    locations = locations.concat(JSON.parse(data));
                    $( "#location-filter" ).autocomplete({
                        source: locations,
                        messages: {
                            noResults: 'no results',
                            results: function (obj) {
                                return ''
                            }
                        }
                    }).on('focus', function () {
                        $( "#location-filter" ).autocomplete('search', ' ');
                    }).on('search', function () {
                        if ($( "#location-filter" ).val() === '') {
                            $( "#location-filter" ).autocomplete('search', ' ');
                        }
                    });
                });
            });
        });

        /*
        Date
        */

        function reloadDate(){
            $( "#dateStart" ).val('');
            $( "#dateEnd" ).val('');
            $( "#dateStart" ).datepicker();
            $( "#dateEnd" ).datepicker();
            $("#dateStart").datepicker("destroy");
            $("#dateEnd").datepicker("destroy");
            $( "#dateStart" ).datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: new Date('<?=$bornes_date[0]?>'),
                maxDate: new Date('<?=$bornes_date[1]?>'),
                onSelect: function(dateText) {
                    $('#dateEnd').datepicker({
                        dateFormat: 'yy-mm-dd',
                        minDate: dateText,
                        maxDate: new Date('<?=$bornes_date[1]?>'),
                        beforeShowDay: function(date){
                            var current = jQuery.datepicker.formatDate('yy-mm-dd', date);
                            return [ datesReserve.indexOf(current) == -1 ]
                        }
                    });
                },
                beforeShowDay: function(date){
                    var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                    return [ datesReserve.indexOf(string) == -1 ]
                }
            });
        }
        $( function() {
            reloadDate();
        });
        /*
        Select Search Box
        */
        $('input[data-list]').each(function () {
            var availableTags = $('#' + $(this).attr("data-list")).find('option').map(function () {
                return this.value;
            }).get();
            $(this).autocomplete({
                source: availableTags,
                messages: {
                    noResults: 'no results',
                    results: function (obj) {
                        return ''
                    }
                }
            }).on('focus', function () {
                $(this).autocomplete('search', ' ');
            }).on('search', function () {
                if ($(this).val() === '') {
                    $(this).autocomplete('search', ' ');
                }
            });
        });

        /*
        Wizard Form
         */
        var current_fs, next_fs, previous_fs;
        var left, opacity, scale;
        var animating;
        $(".next").click(function(){
            if(animating) return false;
            animating = true;
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            next_fs.show();
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    scale = 1 - (1 - now) * 0.2;
                    left = (now * 50)+"%";
                    opacity = 1 - now;
                    current_fs.css({
                        'transform': 'scale('+scale+')',
                        'position': 'absolute'
                    });
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 800,
                complete: function(){
                    current_fs.hide();
                    animating = false;
                },
                easing: 'easeInOutBack'
            });
        });
        $(".previous").click(function(){
            if(animating) return false;
            animating = true;
            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
            previous_fs.show();
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    scale = 0.8 + (1 - now) * 0.2;
                    left = ((1-now) * 50)+"%";
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
                },
                duration: 800,
                complete: function(){
                    current_fs.hide();
                    animating = false;
                },
                easing: 'easeInOutBack'
            });
        });
    </script>
<?php
require_once('template/footer.php');
?>