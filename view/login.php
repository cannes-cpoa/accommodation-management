<?php
require_once('template/head.php');
?>
    <div class="login">
        <form class="form-signin" method="POST">
            <h1><?=WEBSITE_NAME?></h1>
            <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
            <?=$alert?>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>
    </div>
<?php
require_once('template/footer.php');
?>