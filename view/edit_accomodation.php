<?php
require_once('template/head.php');
?>
    <div class="container-fluid section">
        <h2>Bienvenue M. <?=htmlspecialchars(strtoupper($_SESSION['USER']->getLastName()))?>,</h2>
        <h3>Ici vous pouvez éditer votre hébérgement.</h3>
    </div>
    <div class="container-fluid section">
        <form action="" method="POST">
            <div class="row justify-content-md-center">
                <?=$alert?>
                <div class="col col-lg-3">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Donnez un nom à votre hebergement</label>
                        <input type="text" class="form-control" name="accomodationName" id="accomodationName" aria-describedby="accomodationName" placeholder="Le X" value="<?=htmlspecialchars($accomodation->getName())?>">
                        <!-- <small id="accomodationName" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Code Postal</label>
                        <input type="text" class="form-control" name="postalCode" id="postalCode" placeholder="69100" value="<?=htmlspecialchars($accomodation->getPostalCode())?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Adresse</label>
                        <input type="text" class="form-control" name="address" id="address" placeholder="" value="<?=htmlspecialchars($accomodation->getAddress())?>">
                    </div>
                </div>
                <div class="col-md-auto"></div>
                <div class="col col-lg-3">
                    <div class="form-group">
                        <label for="typeSelect">Sélectionnez un type d'hébergement</label>
                        <select class="form-control" name="accomodationType" id="typeSelect">
                            <?php
                            foreach ($accomodationType as $type){
                                ?>
                                <option<?php if($type === $accomodation->getType()){ echo ' selected="selected"'; } ?>><?=$type?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="typeSelect">Quels services proposez-vous ?</label><br>
                        <?php
                        $c = 0;
                        $accomService = $accomodation->getServices();
                        foreach ($services as $service){
                            $c++;
                            ?><div class="form-check form-check-inline">
                            <input <?php if(in_array($service->getName(),$accomService)){ echo 'checked'; } ?> class="form-check-input" type="checkbox" name='accomodationService[]' id="inlineCheckbox<?=$c?>" value="<?=htmlspecialchars($service->getName())?>">
                            <label class="form-check-label" for="inlineCheckbox<?=$c?>"><?=htmlspecialchars($service->getName())?></label>
                            </div>
                            <?php
                        }
                        ?>
                    </div><br>
                    <button type="submit" class="btn btn-primary">Modifier</button>
                </div>
            </div>
        </form>
    </div>
<?php
require_once('template/footer.php');
?>