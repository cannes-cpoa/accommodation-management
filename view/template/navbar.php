<?php

?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="<?=genURL('index');?>"><?= WEBSITE_NAME; ?></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <?php
                if($_SESSION['USER']->isLoggedIn()){
                    switch($_SESSION['USER']->getType()){
                        case 'AccomodationOwner':
                            if(!$_SESSION['USER']->getAccomodationId()){
                                echo navItem('Ajout Hebergement',genURL('accomodation/add'));
                            } else {
                                echo navItem('Gestion des disponibilités', genURL('accomodation/date'));
                                echo navItem('Edition de l\'hebergement', genURL('accomodation/edit'));
                            }
                            break;
                        case 'Staff':
                            echo navItem('Gestion des réservations',genURL('manager'));
                            echo navItem('Gestion des VIP',genURL('vip'));
                            break;
                    }
                }else{
                    echo navItem('Login',genURL('login'));
                }
                ?>

            </ul>
            <?php
            if($_SESSION['USER']->isLoggedIn()){
            ?>
            <ul class="navbar-nav d-flex flex-row">
                <li class="nav-item me-3 me-lg-0 dropdown">
                    <a
                            class="nav-link dropdown-toggle"
                            href="#"
                            id="navbarDropdown"
                            role="button"
                            data-mdb-toggle="dropdown"
                            aria-expanded="false"
                    >
                        <i class="fas fa-user"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><h6 class="dropdown-header"><?=htmlspecialchars(strtoupper($_SESSION['USER']->getLastName()))?> <?=htmlspecialchars($_SESSION['USER']->getFirstName())?></h6></li>
                        <!-- <li><h6 class="dropdown-header"><?=htmlspecialchars(strtoupper($_SESSION['USER']->getType()))?></h6></li> -->
                        <li><hr class="dropdown-divider" /></li>
                        <li>
                            <a class="dropdown-item" href="<?=genURL('logout')?>">Déconnexion</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <?php
            }
            ?>
        </div>
    </div>
</nav>