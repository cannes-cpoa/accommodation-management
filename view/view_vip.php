<?php
require_once('template/head.php');
?>
    <div class="container-fluid section">
        <h2>Gestions des réservations de "<?=htmlspecialchars($select_user->getEmail())?>"</h2>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Nom de logement</th>
                <th scope="col">Nombre d'occupants</th>
                <th scope="col">Nombre de chambres</th>
                <th scope="col">Date d'arrivée</th>
                <th scope="col">Date de sortie</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            forEach($reservations as $reservation){
                $acom = Accomodation::fetchById($reservation->getAccomodationId());
                ?>
                <tr>
                    <th scope="row"><a href="<?=genURL('manager/' . $acom->getId() . '/view')?>"><?=htmlspecialchars($acom->getName())?></a></th>
                    <td><?=$reservation->getPeopleCount()?></td>
                    <td><?=$reservation->getRoomCount()?></td>
                    <td><?=$reservation->getStartDate()?></td>
                    <td><?=$reservation->getEndDate()?></td>
                    <td><button onclick="deleteReservation('<?=$reservation->getAccomodationId()?>', '<?=$reservation->getStartDate()?>')" class="btn btn-danger">Supprimer</button></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <script>
        function deleteReservation(id, date){
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange=function()
            {
                if (xhr.readyState==4 && xhr.status==200)
                {
                    if(xhr.responseText == 'ok'){
                        location.reload();
                    }
                }
            }
            xhr.open("POST", "<?=genURL('vip/') . $reservation->getUserEmail() . '/delete'?>", true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.send('id=' + id + '&start=' + date);
        }
    </script>
<?php
require_once('template/footer.php');
?>