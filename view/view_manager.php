<?php
require_once('template/head.php');
?>
    <div class="container-fluid section">
        <h2>Gestions des réservations de "<?=htmlspecialchars($hotel->getName())?>"</h2>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Email VIP</th>
                <th scope="col">Nombre d'occupants</th>
                <th scope="col">Nombre de chambres</th>
                <th scope="col">Date d'arrivée</th>
                <th scope="col">Date de sortie</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
                forEach($reservations as $reservation){
                    ?>
                    <tr>
                        <th scope="row"><a href="<?=genURL('vip/' . $reservation->getUserEmail() . '/view')?>"><?=$reservation->getUserEmail()?></a></th>
                        <td><?=$reservation->getPeopleCount()?></td>
                        <td><?=$reservation->getRoomCount()?></td>
                        <td><?=$reservation->getStartDate()?></td>
                        <td><?=$reservation->getEndDate()?></td>
                        <td><button onclick="deleteReservation('<?=$reservation->getUserEmail()?>', '<?=$reservation->getStartDate()?>')" class="btn btn-danger">Supprimer</button></td>
                    </tr>
                        <?php
                }
            ?>
            </tbody>
        </table>
    </div>
<script>
    function deleteReservation(email, date){
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange=function()
        {
            if (xhr.readyState==4 && xhr.status==200)
            {
                if(xhr.responseText == 'ok'){
                    location.reload();
                }
            }
        }
        xhr.open("POST", "<?=genURL('manager/') . $hotel->getId() . '/delete'?>", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send('email=' + email + '&start=' + date);
    }
</script>
<?php
require_once('template/footer.php');
?>