<?php
require_once('template/head.php');
?>
    <div class="container-fluid section">
        <div class="row">
            <div class="col-md-10"><h2>Gestions des VIP</h2></div>
            <div class="col-md-2"><a href="<?=genURL('reservation')?>" class="btn btn-success">Ajouter une Reservation</a></div>
        </div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Email</th>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Téléphone</th>
                <th scope="col">Nombre de Reservation(s)</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($users as $user){
                ?>
                <tr>
                    <th scope="row"><?=$user->getEmail()?></th>
                    <td><?=htmlspecialchars($user->getFirstName())?></td>
                    <td><?=htmlspecialchars($user->getLastName())?></td>
                    <td><?=htmlspecialchars($user->getPhoneNumber())?></td>
                    <td><?=count(AccomodationReservation::fetchByUserEmail($user->getEmail()))?></td>
                    <td><a href="<?=genURL('vip/' . $user->getEmail() . '/view')?>" class="btn btn-primary">Liste des reservations</a></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
<?php
require_once('template/footer.php');
?>