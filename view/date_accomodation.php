<?php
require_once('template/head.php');
?>
    <div class="container-fluid section">
        <form action="" method="POST">
            <div class="card border border-dark mb-3">
                <div class="card-body text-dark">
                    <h3 class="card-title">Gestion des disponibilités de "<?= $accomodation->getName() ?>"</h3>
                    <div style="margin: auto;">
                        <?php
                        echo $alert;
                        $rowCount = 0;
                        $maxPerRow = 4;
                        $mdCalc = ceil(12 / $maxPerRow);
                        foreach ($allRange as $date => $vol) {
                            if ($rowCount == 0) {
                                echo '<div class="row" style="padding-top: 15px">';
                            }
                            ?>
                            <div class="col-md-<?= $mdCalc ?>">
                                <label for="<?= $date; ?>"><?=dateToFrench($date, "j F Y");?></label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="lit<?= $date; ?>">Nb de Lits</span>
                                    <input type="text" class="form-control" placeholder="0" aria-label="lit<?= $date; ?>" name="<?= $date; ?>-beds" value="<?=$vol['beds']?>">
                                </div>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="<?= $date; ?>">Nb de Chambres</span>
                                    <input type="text" class="form-control" placeholder="0" aria-label="<?= $date; ?>" name="<?= $date; ?>-rooms" value="<?=$vol['rooms']?>">
                                </div>
                            </div>
                            <?php
                            $rowCount++;
                            if ($rowCount == $maxPerRow) {
                                echo '</div>';
                                $rowCount = 0;
                            }
                        }
                        ?>
                    </div>
                </div>
                <button type="submit" class="btn-success"><h1>VALIDER</h1></button>
            </div>
        </form>
    </div>
<?php
require_once('template/footer.php');
?>