<?php
require_once('config/config.php');
require_once('src/func.php');
require_once('src/model.php');
session_start();
/*
 * Env path
 */
define('__PATH', str_replace(WEBSITE_PATH, '', $_SERVER['REQUEST_URI']));
/*
 * User session
 */

if(!isset($_SESSION['USER'])){
    $_SESSION['USER'] = new User();
}else{
    /*
     * Init DB login
     */
    Model::initDatabase();
    /*
     * Check if user still exist
     */
    $_SESSION['USER']->refresh();
}

/*
 * Routing
 */
$path = explode('/',explode('?',__PATH)[0]);
$assetsLevel = count($path)-1;
switch($path[0]){
    case '':
        $path[0] = WEBSITE_DEFAULT_PATH;
        require_once(CONTROLLER_PATH.WEBSITE_DEFAULT_PATH.'.php');
        break;
    case (is_file(CONTROLLER_PATH. $path[0] .'.php')):
        require_once(CONTROLLER_PATH.$path[0].'.php');
        break;
    default:
        require_once(CONTROLLER_PATH.'404.php');
}
