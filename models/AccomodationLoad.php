<?php


class AccomodationLoad extends Model
{
    private $data;

    public function __construct($data = null)
    {
        parent::__construct();
        $_col = get_class()::getColumns();
        if($data !== null){
            forEach($data as $key=>$value){
                if(!key_exists($key, $_col)){
                    throw new Exception('Invalid data entry');
                }else{
                    $this->data[$key] = $value;
                }
            }
        }
        return $this;
    }
    /*
     * Getters
     */
    public function getAccomodationId(): string
    {
        if(isset($this->data['AccomodationId']))
            return $this->data['AccomodationId'];
        return false;
    }
    public function getDate(): string
    {
        if(isset($this->data['date']))
            return $this->data['date'];
        return false;
    }
    public function getBedCount(): string
    {
        if(isset($this->data['beds']))
            return $this->data['beds'];
        return false;
    }
    public function getRoomCount(): string
    {
        if(isset($this->data['rooms']))
            return $this->data['rooms'];
        return false;
    }

    public static function getByAccomodationId($AccomodationId)
    {
        $output = AccomodationLoad::getRange();
        $data = AccomodationLoad::fetch(array(['AccomodationId','=', $AccomodationId]));
        forEach($data as $acomLoad){
            if(key_exists($acomLoad->getDate(),$output)){
                $output[$acomLoad->getDate()] = ["beds" => (int)$acomLoad->getBedCount(), "rooms" => (int)$acomLoad->getRoomCount()];
            }
        }
        return $output;
    }
    /*
     * Setters
     */
    public static function setAccomodationLoad($AccomodationId, $data)
    {
        $toUpdate = [];
        $toInsert = [];
        $currentDBs = [];

        /*
         * Get in DBs
         */
        $ranges = AccomodationLoad::getRange();
        $dataLoad = AccomodationLoad::fetch(array(['AccomodationId','=', $AccomodationId]));
        forEach($dataLoad as $acomLoad){
            if(key_exists($acomLoad->getDate(),$ranges)){
                $currentDBs[$acomLoad->getDate()] = ["beds" => (int)$acomLoad->getBedCount(), "rooms" => (int)$acomLoad->getRoomCount()];
            }
        }

        /*
         * Fill Update/Insert List
         */
        forEach($data as $date=>$updatedValue){
            if(key_exists($date, $currentDBs)){
                $toUpdate[$date] = $updatedValue;
            }
            $toInsert[$date] = $updatedValue;
        }
        /*
         * Building Queries
         */
        // Delete the UpdateList
        $q_delete = 'DELETE FROM AccomodationLoad WHERE AccomodationId = ' .(int)$AccomodationId. ' AND date IN (' .join(', ', array_map(function ($i){ return "'" . $i . "'"; },array_keys($toUpdate))). ')';
        // Insert the Items
        $q_insert = 'INSERT INTO AccomodationLoad VALUES ' . str_replace('%AccomodationId%' , $AccomodationId, join(', ', array_map(function ($i){ return "(" . $i . ")"; },array_map(function ($i,$toInsert){ return "%AccomodationId% , '" . $i . "', " .$toInsert['beds']. ", " . $toInsert['rooms'];},array_keys($toInsert),$toInsert))));
        if(count($toUpdate) > 0){
            $q = AccomodationLoad::$db->prepare($q_delete);
            $q->execute();
        }
        if(count($toInsert) > 0){
            $q = AccomodationLoad::$db->prepare($q_insert);
            return($q->execute() == true);
        }
        return false;
    }

    public static function getRange(){
        $dates = [date_create("2021-05-11"), date_create("2021-05-23")];
        $output = [];
        $period = new DatePeriod(
            $dates[0],
            new DateInterval('P1D'),
            $dates[1]
        );
        foreach ($period as $key => $value) {
            $output[$value->format('Y-m-d')] = ["beds" => 0, "rooms" => 0];
        }
        return $output;
    }
}