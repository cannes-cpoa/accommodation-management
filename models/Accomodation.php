<?php


class Accomodation extends Model
{
    private $data;

    public function __construct($data = null)
    {
        parent::__construct();
        $_col = get_class()::getColumns();
        if($data !== null){
            forEach($data as $key=>$value){
                if(!key_exists($key, $_col)){
                    throw new Exception('Invalid data entry');
                }else{
                    $this->data[$key] = $value;
                }
            }
        }
        return $this;
    }
    public function getId()
    {
        return $this->data['id'];
    }
    public function getName()
    {
        return $this->data['name'];
    }
    public function getAddress()
    {
        return $this->data['address'];
    }
    public function getPostalCode()
    {
        return $this->data['postalCode'];
    }
    public function getType()
    {
        return $this->data['type'];
    }
    public function getServices()
    {
        $out = [];
        if(isset($this->data['id'])){
            $query = 'SELECT AccomodationServicesName FROM _AccomodationServices WHERE AccomodationId = ?;';
            $q = Accomodation::$db->prepare($query);
            $q->execute([$this->data['id']]);
            forEach($q->fetchAll(PDO::FETCH_ASSOC) as $item){
                $out[] = $item['AccomodationServicesName'];
            }
            return $out;
        }
        return false;
    }

    public static function insertUser(User $user, $data): bool
    {
        $inserted = Accomodation::insert($data);
        if($inserted !== false){
            /*
             * User foreign key
             */
            $query = 'UPDATE User SET AccomodationId=? WHERE email=?;';
            $q = Accomodation::$db->prepare($query);
            return ($q->execute([Accomodation::$db->lastInsertId(), $user->getEmail()]) == true);
        }
        return false;
    }

    public static function fetchByUser(User $user)
    {
        if($user->getAccomodationId()) {
                $data = Accomodation::fetch(array(['id','=', $user->getAccomodationId()]));
                if(count($data) === 1){
                    return $data[0];
                }
        }
        return false;
    }
    public static function fetchById(int $id)
    {
            $data = Accomodation::fetch(array(['id','=', $id]));
            if(count($data) === 1){
                return $data[0];
            }
        return false;
    }
    public static function fetchByName(string $name)
    {
        $data = Accomodation::fetch(array(['name','=', $name]));
        if(count($data) === 1){
            return $data[0];
        }
        return false;
    }
    public function setServices(array $names)
    {
        /*
         * Clear & Add
         */
        if(isset($this->data['id'])){
            $query = 'DELETE FROM _AccomodationServices WHERE AccomodationId = ?;';
            $q = Accomodation::$db->prepare($query);
            if($q->execute([$this->data['id']])){
                /*
                 * Add
                 */
                if(!empty($names)){
                    $args = [];
                    $insertquery = 'INSERT INTO _AccomodationServices VALUES ';
                    $s = array_fill(0, count($names), '(?,?)');
                    $insertquery .= join(',', $s) . ';';
                    forEach($names as $name){
                        $args[] = $this->data['id'];
                        $args[] = $name;
                    }
                    $q = Accomodation::$db->prepare($insertquery);
                    return ($q->execute($args) == true);
                }
                return true;
            }
        }
        return false;
    }
}