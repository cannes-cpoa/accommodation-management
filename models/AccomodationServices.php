<?php


class AccomodationServices extends Model
{
    private $data;

    public function __construct($data = null)
    {
        parent::__construct();
        $_col = get_class()::getColumns();
        if($data !== null){
            forEach($data as $key=>$value){
                if(!key_exists($key, $_col)){
                    throw new Exception('Invalid data entry');
                }else{
                    $this->data[$key] = $value;
                }
            }
        }
        return $this;
    }
    public static function getAll(): array
    {
        $out = [];
        forEach(AccomodationServices::fetch() as $item){
            $out[] = $item->getName();
        }
        return $out;
    }

    public function getName(): string
    {
        if(isset($this->data['name']))
            return $this->data['name'];
        return false;
    }
}