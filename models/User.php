<?php


class User extends Model
{
    private $data;

    public function __construct($data = null)
    {
        parent::__construct();
        $_col = get_class()::getColumns();
        if($data !== null){
            forEach($data as $key=>$value){
                if(!key_exists($key, $_col)){
                    throw new Exception('Invalid data entry');
                }else{
                    $this->data[$key] = $value;
                }
            }
        }
        return $this;
    }
    /*
     * Getters
     */
    public function getEmail(): string
    {
        if(isset($this->data['email']))
            return $this->data['email'];
        return false;
    }
    public function getFirstName(): string
    {
        if(isset($this->data['firstName']))
            return $this->data['firstName'];
        return false;
    }
    public function getLastName(): string
    {
        if(isset($this->data['lastName']))
            return $this->data['lastName'];
        return false;
    }
    public function getPhoneNumber(): string
    {
        if(isset($this->data['phoneNumber']))
            return $this->data['phoneNumber'];
        return false;
    }
    public function getType(): string
    {
        if(isset($this->data['UserTypeName']))
            return $this->data['UserTypeName'];
        return false;
    }
    public function getAccomodationId(): string
    {
        if(isset($this->data['AccomodationId']))
            return $this->data['AccomodationId'];
        return false;
    }

    public static function fetchByEmail(string $email)
    {
        $data = User::fetch(array(['email','=', $email]));
        if(count($data) === 1){
            return $data[0];
        }
        return false;
    }
    /*
     * Session
     */
    public function refresh(): void
    {
        if (isset($this->data['email'])){
            $exist = User::fetch(array(['email','=',$this->data['email']]));
            if(count($exist) === 1){
                $_SESSION['USER'] = $exist[0];
            }else{
                /*
                 * Account must have been deleted
                 */
                $_SESSION = array();
                session_destroy();
            }
        }
    }
    public function isLoggedIn(): bool
    {
        return (isset($this->data['email']));
    }
}