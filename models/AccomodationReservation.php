<?php


class AccomodationReservation extends Model
{
    private $data;

    public function __construct($data = null)
    {
        parent::__construct();
        $_col = get_class()::getColumns();
        if($data !== null){
            forEach($data as $key=>$value){
                if(!key_exists($key, $_col)){
                    throw new Exception('Invalid data entry');
                }else{
                    $this->data[$key] = $value;
                }
            }
        }
        return $this;
    }
    /*
     * Getters
     */
    public function getUserEmail(): string
    {
        if(isset($this->data['UserEmail']))
            return $this->data['UserEmail'];
        return false;
    }
    public function getAccomodationId(): string
    {
        if(isset($this->data['AccomodationId']))
            return $this->data['AccomodationId'];
        return false;
    }
    public function getPeopleCount(): string
    {
        if(isset($this->data['peopleCount']))
            return $this->data['peopleCount'];
        return false;
    }
    public function getRoomCount(): string
    {
        if(isset($this->data['roomCount']))
            return $this->data['roomCount'];
        return false;
    }
    public function getStartDate(): string
    {
        if(isset($this->data['startDate']))
            return $this->data['startDate'];
        return false;
    }
    public function getEndDate(): string
    {
        if(isset($this->data['endDate']))
            return $this->data['endDate'];
        return false;
    }

    public static function fetchByAccomodationId(int $id)
    {
        $data = AccomodationReservation::fetch(array(['AccomodationId','=', $id]));
        return $data;
    }
    public static function fetchByUserEmail(string $email){
        $data = AccomodationReservation::fetch(array(['UserEmail','=', $email]));
        return $data;
    }

    public static function create($email, $people, $room, $start, $end, $accomodationId){
        return AccomodationReservation::insert(array(
            'UserEmail'=> $email,
            'AccomodationId'=> $accomodationId,
            'startDate'=> $start,
            'endDate'=> $end,
            'peopleCount'=> $people,
            'roomCount'=> $room,
        ));
    }
    public static function remove($AccomodationId, $email, $startDate){
        return AccomodationReservation::delete(array(['AccomodationId','=',$AccomodationId],['UserEmail','=',$email],['startDate','=',$startDate]));
    }
}