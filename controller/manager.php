<?php
if ($_SESSION['USER']->isLoggedIn() && $_SESSION['USER']->getType() == 'Staff') {
    $alert = '';
    if (isset($path[1],$path[2]) && Accomodation::fetchById($path[1]) !== false) {
        switch ($path[2]) {
            case 'view':
                $hotel = Accomodation::fetchById($path[1]);
                $reservations = AccomodationReservation::fetchByAccomodationId($hotel->getId());
                require_once(VIEW_PATH . $path[2] . '_' . $path[0] . '.php');
                break;
            case 'add':
                break;
            case 'delete':
                if(getPost('email', false) && getPost('start', false)){
                    if(AccomodationReservation::remove($path[1], getPost('email'),getPost('start'))){
                        echo 'ok';
                    }
                }
                break;
            default:
                redirect();
        }
    } else {
            $hotels = Accomodation::fetch();
            require_once(VIEW_PATH . $path[0] . '.php');
    }
} else {
    redirect('login');
}