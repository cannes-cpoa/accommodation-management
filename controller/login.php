<?php

if(!$_SESSION['USER']->isLoggedIn() && !isset($path[1])) {
    $alert = '';
    if(isset($_POST['email'],$_POST['password'])){
        $user = new User;
        $userArray = $user->fetch(
                array(
                    ['email','=',$_POST['email']],
                    ['passwordHash', '=', hash('sha256',$_POST['password'])]
                )
            );
        if(count($userArray) === 1 && in_array($userArray[0]->getType(), ['AccomodationOwner', 'Staff'])){
            /*
             * Successful login
             */
            $_SESSION['USER'] = $userArray[0]; // Pass the returned User type data into Session
            redirect();
        }else{
            /*
             * Error message
             */
            $alert = alert('danger', 'Information incorrectes.');
        }
    }
    require_once(VIEW_PATH . $path[0] . '.php');
}else{
    redirect();
}