<?php
if($_SESSION['USER']->isLoggedIn()) {
    if (!isset($path[1])) {
        require_once(VIEW_PATH . $path[0] . '.php');
    }
}else{
    redirect('login');
}