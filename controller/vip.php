<?php
if ($_SESSION['USER']->isLoggedIn() && $_SESSION['USER']->getType() == 'Staff') {
    $alert = '';
    if (isset($path[1],$path[2]) && User::fetchByEmail($path[1]) !== false) {
        switch ($path[2]) {
            case 'view':
                $select_user = User::fetchByEmail($path[1]);
                $reservations = AccomodationReservation::fetchByUserEmail($select_user->getEmail());
                require_once(VIEW_PATH . $path[2] . '_' . $path[0] . '.php');
                break;
            case 'add':
                break;
            case 'delete':
                if(getPost('id', false) && getPost('start', false)){
                    if(AccomodationReservation::remove(getPost('id'), $path[1],getPost('start'))){
                        echo 'ok';
                    }
                }
                break;
            default:
                redirect();
        }
    } else {
        $users = User::fetch(array(['UserTypeName', '=', 'VIP']));
        require_once(VIEW_PATH . $path[0] . '.php');
    }
} else {
    redirect('login');
}