<?php
if ($_SESSION['USER']->isLoggedIn() && $_SESSION['USER']->getType() == 'AccomodationOwner') {
    $accomodationType = ['Hôtel', 'Gîte', 'Camping', 'Villa en location'];
    $alert = '';
    $availableServices = AccomodationServices::getAll();
    $re_name = '/^[a-zA-Z -\'?*éàèôê0-9"()+&]{1,}$/';
    $re_cp = '/[0-9]{5}/';
    if (isset($path[1])) {
        switch ($path[1]) {
            case 'add':
                if (!$_SESSION['USER']->getAccomodationId()) {
                    /*
                     * Creating Accomodation
                     */
                    if (isset(
                        $_POST['accomodationName'],
                        $_POST['postalCode'],
                        $_POST['address'],
                        $_POST['accomodationType']
                    )) {
                        /*
                         * Checking inputs
                         */


                        if (!preg_match($re_name, $_POST['accomodationName']) || !preg_match($re_cp, $_POST['postalCode']) || ((isset($_POST["accomodationService"]) && gettype($_POST['accomodationService']) === 'array' && array_diff($_POST["accomodationService"], $availableServices)))) {
                            $alert = alert('danger', 'Le formulaire est invalide');
                        } else {
                            if (Accomodation::insertUser($_SESSION['USER'], array(
                                    "name" => $_POST['accomodationName'],
                                    "address" => $_POST['address'],
                                    "postalCode" => $_POST['postalCode'],
                                    "type" => $_POST['accomodationType'])) != false) {
                                /*
                                 * Update users information
                                 */
                                $_SESSION['USER']->refresh();
                                /*
                                 * Insert ok
                                 */
                                $accomodation = Accomodation::fetchByUser($_SESSION['USER']);
                                $accomodation->setServices($_POST["accomodationService"]);
                                $alert = alert('success', 'Ajout du logement réussi.');
                            } else {
                                $alert = alert('danger', 'Erreur lors de l\'insertion du logement.');
                            }
                        }
                    }


                    $services = AccomodationServices::fetch();
                    require_once(VIEW_PATH . $path[1] . '_' . $path[0] . '.php');
                } else {
                    redirect();
                }
                break;
            case 'edit':
                if ($_SESSION['USER']->getAccomodationId()) {
                    $accomodation = Accomodation::fetchByUser($_SESSION['USER']);

                    if (isset(
                        $_POST['accomodationName'],
                        $_POST['postalCode'],
                        $_POST['address'],
                        $_POST['accomodationType']
                    )) {
                        if (!preg_match($re_name, $_POST['accomodationName']) || !preg_match($re_cp, $_POST['postalCode']) || ((isset($_POST["accomodationService"]) && gettype($_POST['accomodationService']) === 'array' && array_diff($_POST["accomodationService"], $availableServices)))) {
                            $alert = alert('danger', 'Le formulaire est invalide');
                        } else {
                            Accomodation::update(array(
                                "name" => $_POST['accomodationName'],
                                "address" => $_POST['address'],
                                "postalCode" => $_POST['postalCode'],
                                "type" => $_POST['accomodationType']), array(['id','=',$accomodation->getId()]));
                            $accomodation = Accomodation::fetchByUser($_SESSION['USER']);
                            $accomodation->setServices($_POST["accomodationService"]);
                            $alert = alert('success', 'Le logement a été modifié.');
                        }
                        $accomodation->setServices($_POST["accomodationService"]);
                    }
                    $services = AccomodationServices::fetch();
                    require_once(VIEW_PATH . $path[1] . '_' . $path[0] . '.php');
                } else {
                    redirect();
                }
                break;
            case 'date':
                if ($_SESSION['USER']->getAccomodationId()) {
                    $allRange = AccomodationLoad::getByAccomodationId($_SESSION['USER']->getAccomodationId());
                    /*
                     * Calcul des minimum / disponibilités
                     */
                    $minimum = AccomodationLoad::getRange();
                    $reservations = AccomodationReservation::fetchByAccomodationId($_SESSION['USER']->getAccomodationId());
                    forEach($reservations as $reservation){
                        $start = date_create($reservation->getStartDate());
                        $end = date_create($reservation->getEndDate());
                        $end->add(new DateInterval('P1D'));
                        $period = new DatePeriod(
                            $start,
                            new DateInterval('P1D'),
                            $end
                        );
                        foreach ($period as $key => $value) {
                            if(in_array($value->format('Y-m-d'), array_keys($minimum))){
                                $minimum[$value->format('Y-m-d')]['rooms'] += $reservation->getRoomCount();
                                $minimum[$value->format('Y-m-d')]['beds'] += $reservation->getPeopleCount();
                            }
                        }
                    }

                    /*
                     * Formulaire POST
                     */
                    $filteredPost = [];
                    $error = false;
                    forEach($_POST as $key=>$value){
                        $parts = explode('-', $key);
                        $last = array_pop($parts);
                        $parts = array(implode('-', $parts), $last);
                        if(array_key_exists($parts[0],$allRange)){
                            /*
                             * Update count
                             */
                            $col = $parts[1];
                            if(in_array($col, ['beds','rooms'])){
                                if($minimum[$parts[0]][$col] > (int)$value){
                                    $error = true;
                                    $alert = alert('danger','Impossible de réduire le compte de \'' .$col. '\' pour le ' . dateToFrench($parts[0], "j F Y") . ' due a des reservations déjà enregistrées.');
                                }else {
                                    $filteredPost[$parts[0]][$col] = (int)$value;
                                }
                            }
                        }
                    }
                    if(count($filteredPost) > 0 && $error == false){
                        AccomodationLoad::setAccomodationLoad($_SESSION['USER']->getAccomodationId(), $filteredPost);
                        $allRange = AccomodationLoad::getByAccomodationId($_SESSION['USER']->getAccomodationId());
                    }

                    $accomodation = Accomodation::fetchByUser($_SESSION['USER']);
                    require_once(VIEW_PATH . $path[1] . '_' . $path[0] . '.php');
                } else {
                    redirect();
                }
                break;
            default:
                redirect();
        }
    } else {
        redirect();
    }
} else {
    redirect('login');
}