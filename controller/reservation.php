<?php
if ($_SESSION['USER']->isLoggedIn() && $_SESSION['USER']->getType() == 'Staff') {
    if(isset($path[1])){
        switch ($path[1]){
            case 'hotel':
                if(isset($path[2],$path[3],$path[4],$path[5])){
                    // 2 = people
                    // 3 = room
                    // 4 = start
                    // 5 = end
                    // Simple checks
                    $start = strtotime($path[4]);
                    $end = strtotime($path[5]);

                    $goodAccomodation = [];
                    if($end >= $start) {
                        $dates_check = [];
                        // Good
                        $start = date_create($path[4]);
                        $end = date_create($path[5]);
                        $end->add(new DateInterval('P1D'));
                        $period = new DatePeriod(
                            $start,
                            new DateInterval('P1D'),
                            $end
                        );
                        foreach ($period as $key => $value) {
                            $dates_check[] = $value->format('Y-m-d');
                        }

                        $accomodations = Accomodation::fetch();

                        forEach($accomodations as $accomodation){
                            $disponibility = AccomodationLoad::getByAccomodationId($accomodation->getId());
                            /*
                             * Calcul minimum / occupants
                             */
                            $minimum = AccomodationLoad::getRange();
                            $reservations = AccomodationReservation::fetchByAccomodationId($accomodation->getId());
                            forEach($reservations as $reservation){
                                foreach ($period as $key => $value) {
                                    if(in_array($value->format('Y-m-d'), array_keys($minimum))){
                                        $minimum[$value->format('Y-m-d')]['rooms'] += $reservation->getRoomCount();
                                        $minimum[$value->format('Y-m-d')]['beds'] += $reservation->getPeopleCount();
                                    }
                                }
                            }

                            forEach($disponibility as $date=>$dispo){
                                if(in_array($date, $dates_check)){
                                    if(((int)$path[2] + (int)$minimum[$date]['beds'] > (int)$dispo['beds']) || ((int)$path[3] + (int)$minimum[$date]['rooms'] > (int)$dispo['rooms'])){
                                        break;
                                    }else{
                                        if($dates_check[count($dates_check) - 1] == $date){
                                            $goodAccomodation[] = $accomodation->getName();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        echo json_encode($goodAccomodation);
                    }
                }
                die();
            case 'date':
                if(isset($path[2])){
                    $dates = [];
                    $reservation = AccomodationReservation::fetchByUserEmail($path[2]);
                    forEach($reservation as $reserv){
                        $start = date_create($reserv->getStartDate());
                        $end = date_create($reserv->getEndDate());
                        $end->add(new DateInterval('P1D'));
                        $period = new DatePeriod(
                            $start,
                            new DateInterval('P1D'),
                            $end
                        );
                        foreach ($period as $key => $value) {
                            if(!in_array($value->format('Y-m-d'), $dates)){
                                $dates[] = $value->format('Y-m-d');
                            }
                        }
                    }
                    echo json_encode($dates);
                }
                die();
            default:
                die();
        }
    }

    $alert = '';
    $bornes_date = array_keys(AccomodationLoad::getRange());
    $bornes_date = [$bornes_date[0], $bornes_date[count($bornes_date) - 1]];
    $vips = User::fetch(array(['UserTypeName', '=', 'VIP']));

    if(getPost('vip',false) && getPost('people',false) && getPost('rooms',false) && getPost('dateStart',false) && getPost('dateEnd',false) && getPost('location',false)){
        $accomodationID = Accomodation::fetchByName(getPost('location',false))->getId();
        AccomodationReservation::create(getPost('vip',false), getPost('people',false), getPost('rooms',false), getPost('dateStart',false), getPost('dateEnd',false), $accomodationID);
        $alert = alert('success','Reservation ajouté');
    }


    require_once(VIEW_PATH . $path[0] . '.php');
} else {
    redirect('login');
}