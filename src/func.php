<?php
/*
 * Getters
 */
function getGet($name, $default = NULL){
    if(isset($_GET[$name])){
        return $_GET[$name];
    }
    return $default;
}
function getPost($name, $default = NULL){
    if(isset($_POST[$name])){
        return $_POST[$name];
    }
    return $default;
}
/*
 * URL & Redirection
 */
function genURL($route = WEBSITE_DEFAULT_PATH){
    return $_SERVER["REQUEST_SCHEME"] . '://' . $_SERVER["SERVER_NAME"] . WEBSITE_PATH . $route;
}
function redirect($route = WEBSITE_DEFAULT_PATH){
    header('Location: ' . genURL($route));
}
/*
 * Front-end render
 */
# Date
function dateToFrench($date, $format)
{
    $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
    $french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
    $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
    return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
}
# Assets path generator
function assetsPath($path, $level = 0){
    return str_repeat('../', $level) . $path;
}
# Alert generator
function alert($status , $msg){
    switch($status){
        case 'primary':
        case 'secondary':
        case 'success':
        case 'danger':
        case 'warning':
        case 'info':
        case 'light':
        case 'dark':
            return "<div class='alert alert-" .$status. "' role='alert'>" .htmlspecialchars($msg). "</div>";
            break;
        default:
            throw new \Exception("Status d'alerte invalide");
    }
}
# Navbar button render
function navItem($name, $path){
    $acc = '';
    if((substr($_SERVER["REQUEST_SCHEME"] . '://' . $_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI'], 0, strlen($path)) === $path) || ($path === ($_SERVER["REQUEST_SCHEME"] . '://' . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]))){
        $acc = 'active';
    }
    return '<li class="nav-item"><a class="nav-link ' .$acc. '" aria-current="page" href="' .htmlspecialchars($path). '">' .htmlspecialchars($name). '</a></li>';
}
